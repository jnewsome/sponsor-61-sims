#!/usr/bin/env pypy3

import sys
import re
import math

circs = {}
RELAY_PAYLOAD_SIZE = 498

class Stat:
  def __init__(self):
    self.min = 0xffffffff
    self.max = 0
    self.total = 0.0
    self.total_sq = 0.0
    self.count = 0

  def avg(self):
    if not self.count:
      return 0
    return self.total/self.count

  # XXX: This might have precision issues:
  # https://nullbuffer.com/articles/welford_algorithm.html#references
  # https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  def dev(self):
    if not self.count:
      return 0
    mean = self.avg()
    var = self.total_sq/self.count - mean*mean
    #if var < 0:
    #  print("Var: "+str(var)+", Mean: "+str(mean)+", total: "+str(self.total)+", total_sq: "+str(self.total_sq)+", count: "+str(self.count))

    return math.sqrt(abs(self.total_sq/self.count - mean*mean))

  def update(self, stat):
    if self.min > stat:
      self.min = stat
    if self.max < stat:
      self.max = stat
    self.total += stat
    self.total_sq += float(stat)*stat
    self.count += 1

  def __str__(self):
    if self.count == 0:
      return "No circuits"
    return "{0:.1f}/{1:.1f}+{2:.1f}/{3:.1f}".format(self.min,self.avg(),self.dev(),self.max)

class MetaStat:
   def __init__(self):
     self.min = Stat()
     self.max = Stat()
     self.avg = Stat()
     self.dev = Stat()

   def update(self, stat):
     self.min.update(stat.min)
     self.max.update(stat.max)
     self.avg.update(stat.avg())
     self.dev.update(stat.dev())

   def __str__(self):
     return "Min: {0:s}, Avg: {1:s}, Dev: {2:.1f}, Max: {3:s}".format(str(self.min), str(self.avg), self.dev.avg(), str(self.max))

class CircPhase:
  def __init__(self):
    self.BWE = Stat()
    self.QUSE = Stat()
    self.CWND = Stat()
    self.RTTFactor = Stat()
    self.minRTT = None
    self.init_minRTT = None
    self.final_CWND = 0
    self.final_QUSE = 0
    self.updates = 0

  def update(self, minRTT, currRTT, CWND, QUSE, BWE):
    self.updates += 1

    if self.init_minRTT == None:
      self.init_minRTT = minRTT

    if self.minRTT == None or self.minRTT > minRTT:
      self.minRTT = minRTT

    if QUSE > 0:
      self.QUSE.update(QUSE)
      self.final_QUSE = QUSE

    if BWE > 0:
      self.BWE.update(BWE)

    self.CWND.update(CWND)
    self.final_CWND = CWND

    self.RTTFactor.update(float(currRTT)/minRTT)

class Circ:
  def __init__(self):
    self.slow_start = CircPhase()
    self.lifetime = CircPhase()
    self.post_4mb = CircPhase()
    self.post_ss = CircPhase()
    self.SS = 1
    self.hitMinCWND = 0
    self.updates = 0
    self.cells = 0
    self.sent_4mb = False

  def update(self, minRTT, currRTT, CWND, INFL, QUSE, BWE, SS):
    self.updates += 1

    self.cells += INFL

    if self.cells*RELAY_PAYLOAD_SIZE > 4*1024*1024:
      self.sent_4mb = True

    if self.SS and not SS:
      self.slow_start.final_CWND = self.lifetime.final_CWND
      if QUSE > 0:
        self.slow_start.final_QUSE = QUSE

    self.SS = SS

    self.lifetime.update(minRTT, currRTT, CWND, QUSE, BWE)

    if self.sent_4mb:
      self.post_4mb.update(minRTT, currRTT, CWND, QUSE, BWE)

    if self.SS:
      self.slow_start.update(minRTT, currRTT, CWND, QUSE, BWE)
    else:
      self.post_ss.update(minRTT, currRTT, CWND, QUSE, BWE)


class PhaseStat:
  def __init__(self, name):
    self.BWE = MetaStat()
    self.CWND = MetaStat()
    self.QUSE = MetaStat()
    self.RTTFactor = MetaStat()
    self.final_CWND = Stat()
    self.final_QUSE = Stat()
    self.num_circs = 0
    self.init_minRTT = Stat()
    self.phase_minRTT = Stat()
    self.name = name

  def update(self, c, cphase):
    if cphase.updates == 0:
      return

    if cphase.BWE.count > 0:
      self.BWE.update(cphase.BWE)
    self.CWND.update(cphase.CWND)
    if cphase.QUSE.count > 0:
      self.QUSE.update(cphase.QUSE)
    self.RTTFactor.update(cphase.RTTFactor)
    self.final_CWND.update(cphase.final_CWND)
    self.final_QUSE.update(cphase.final_QUSE)
    self.init_minRTT.update(float(cphase.init_minRTT)/c.lifetime.minRTT)
    self.phase_minRTT.update(float(cphase.minRTT)/c.lifetime.minRTT)
    self.num_circs += 1

  def print_stats(self):
    print("  Stats for phase "+self.name+":")
    if self.num_circs == 0:
      print("    No circuits")
      return
    print("    BWE: "+str(self.BWE))
    print("    CWND: "+str(self.CWND))
    print("    QUSE: "+str(self.QUSE))
    print("    final CWND: "+str(self.final_CWND))
    print("    final QUSE: "+str(self.final_QUSE))
    print("    RTTFactor: "+str(self.RTTFactor))
    print("    init minRTTFactor: "+str(self.init_minRTT))
    print("    phase minRTTFactor: "+str(self.phase_minRTT))
    print("    Total circs: "+str(self.num_circs))


class NetworkStat:
  def __init__(self, name):
    self.ss = PhaseStat("slow start")
    self.post_ss = PhaseStat("post-slow start")
    self.post_4mb = PhaseStat("post-4mb")
    self.lifetime = PhaseStat("lifetime")
    self.num_circs = 0
    self.num_circs_ss = 0
    self.name = name

  def update(self, c):
    self.ss.update(c, c.slow_start)
    self.lifetime.update(c, c.lifetime)

    if not c.SS:
      self.post_ss.update(c, c.post_ss)
    if c.sent_4mb:
      self.post_4mb.update(c, c.post_4mb)

    self.num_circs += 1
    if c.SS:
      self.num_circs_ss += 1

  def print_stats(self):
    print("\nNetwork statistics for "+self.name+":")
    if self.num_circs == 0:
      print("  No circuits")
      return
    self.post_4mb.print_stats()
    self.ss.print_stats()
    self.post_ss.print_stats()
    self.lifetime.print_stats()

    print("  Total circs: "+str(self.num_circs))
    print("  Circs still in SS: "+str(self.num_circs_ss))

def compute_stats():
  netstats = NetworkStat("all circuits")
  netstats_min = NetworkStat("circuits that hit min cwnd")
  netstats_4mb = NetworkStat("circuits >= 4MB xmit")
  netstats_SS = NetworkStat("circuits never exiting slow start")

  for c in circs.values():
    netstats.update(c)

    if c.hitMinCWND:
      netstats_min.update(c)

    if c.cells*RELAY_PAYLOAD_SIZE > 4*1024*1024:
      netstats_4mb.update(c)

    if c.SS:
      netstats_SS.update(c)

  netstats_4mb.print_stats()
  netstats.print_stats()
  netstats_SS.print_stats()
  netstats_min.print_stats()

f = open(sys.argv[1])
for l in f:
  circ_id_re = re.search(" ([^ ]+) CIRC=([^ ]+) ", l).groups()
  circ_id = circ_id_re[0]+circ_id_re[1]

  if "CC: TOR_VEGAS RTT:" in l or \
     "CC: TOR_VEGAS Onion Circuit" in l:
    if not circ_id in circs:
      circs[circ_id] = Circ()

  if not circ_id in circs:
    continue

  c = circs[circ_id]

  if "Congestion control window hit minimum" in l:
    c.hitMinCWND = 1

  if "CC: TOR_VEGAS" in l:
    # CC: TOR_VEGAS RTT: 227, 234, 234, CWND: 90, INFL: 60, VBDP: 58, QUSE: 2, BWE: 197, SS: 1
    (minRTT, currRTT, maxRTT, CWND, INFL, QUSE, BWE, SS) = \
      map(int,
          re.search("RTT: ([\d]+), ([\d]+), ([\d]+), CWND: ([\d]+), INFL: ([\d]+), VBDP: [\d]+, QUSE: ([\d]+), BWE: ([\d]+), SS: ([\d]+)",
                    l).groups())
    c.update(minRTT, currRTT, CWND, INFL, QUSE, BWE, SS)

compute_stats()

