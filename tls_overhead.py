#!/usr/bin/env pypy3

import sys
import re
import math

f = open(sys.argv[1])

class Stat:
  def __init__(self):
    self.min = 0xffffffff
    self.max = 0
    self.total = 0.0
    self.total_sq = 0.0
    self.count = 0

  def avg(self):
    if not self.count:
      return 0
    return self.total/self.count

  # XXX: This might have precision issues:
  # https://nullbuffer.com/articles/welford_algorithm.html#references
  # https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  def dev(self):
    if not self.count:
      return 0
    mean = self.avg()
    var = self.total_sq/self.count - mean*mean
    #if var < 0:
    #  print("Var: "+str(var)+", Mean: "+str(mean)+", total: "+str(self.total)+", total_sq: "+str(self.total_sq)+", count: "+str(self.count))

    return math.sqrt(abs(self.total_sq/self.count - mean*mean))

  def update(self, stat):
    if self.min > stat:
      self.min = stat
    if self.max < stat:
      self.max = stat
    self.total += stat
    self.total_sq += float(stat)*stat
    self.count += 1

  def __str__(self):
    if self.count == 0:
      return "No circuits"
    return "{0:.2f}/{1:.2f}+{2:.1f}/{3:.2f}".format(self.min,self.avg(),self.dev(),self.max)

tls_stat = Stat()

for l in f:
  if not "Average packaged cell fullness" in l:
    continue

  # TLS write overhead: 13.047965%
  tls_overhead = \
      float(
          re.search("TLS write overhead: ([^%]+)%",
                    l).groups()[0]
           )
  tls_stat.update(tls_overhead)

print("TLS overhead in "+sys.argv[1]+" min/avg+dev/max: "+str(tls_stat))
