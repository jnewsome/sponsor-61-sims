#!/usr/bin/env python3

import glob
import logging
import os
import random
import re
import resource
import shlex
import shutil
import subprocess
import sys
import time
import networkx
import yaml

from logging import debug, info, warning, error

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s')

BASH=shutil.which('bash')

OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

def cmd_shell_string(cmd):
    if type(cmd) == str:
        return cmd
    else:
        return ' '.join(map(shlex.quote, cmd))

def popen(cmd, **kwargs):
    """Run a shell command"""
    if type(cmd) == str:
        kwargs.setdefault('shell', True)
        kwargs.setdefault('executable', BASH)
        # Enable error checking in the shell before running the command.
        cmd = 'set -euo pipefail; ' + cmd

    return subprocess.Popen(cmd, **kwargs)

def run(cmd):
    """Run a shell command"""
    info(OKGREEN + f"$ {cmd_shell_string(cmd)}" + ENDC)
    process = popen(cmd)
    if process.wait() != 0:
        raise RuntimeError(f"Return code {process.returncode}")

def bg(cmd):
    """Run a shell command in the background"""
    info(OKGREEN + f"$ {cmd_shell_string(cmd)} &" + ENDC)

    if os.fork() == 0:
        process = popen(cmd)
        returncode = process.wait()
        warning(f"{WARNING}BG process exited with {returncode}: {cmd_shell_string(cmd)}{ENDC}")
        sys.exit(0)

def check_output(cmd, **kwargs):
    """Run a shell command, capturing output"""
    kwargs.setdefault('encoding', 'utf-8')
    kwargs.setdefault('stdout', subprocess.PIPE)

    process = popen(cmd, **kwargs)
    if process.wait() != 0:
        raise RuntimeError(f"Return code {process.returncode}")
    return process.stdout.read()

def getenv(v, default=None):
    """Get environment variable, or throw if not set and no default"""
    res = os.getenv(v, default)
    if res is None:
        raise NameError(v)
    return res

def apply_bandwidth_overrides(shadow_config_path, overrides):
    config = yaml.load(open(shadow_config_path), yaml.loader.Loader)
    for (host, host_config) in config['hosts'].items():
        for (host_prefix, prefix_overrides) in overrides.items():
            up = prefix_overrides.get('up')
            down = prefix_overrides.get('down')
            if host.startswith(host_prefix):
              if up:
                host_config['bandwidth_up'] = up
              if down:
                host_config['bandwidth_down'] = down
    yaml.dump(config, stream=open(shadow_config_path, 'w'))

def apply_torv2_overrides(shadow_config_path, host_pattern=None, torv2_bw_up_frac=None, ci_project_dir=None):
    config = yaml.load(open(shadow_config_path), yaml.loader.Loader)

    host_pattern = re.compile(host_pattern)

    hosts = []
    total_bw = 0.0
    for (host, host_config) in config['hosts'].items():
        if not host_pattern.match(host):
            continue
        (bw, unit) = host_config['bandwidth_up'].split()
        # We only handle everything using the same unit
        assert(unit == 'kilobit')
        total_bw += float(bw)
        hosts.append(host)

    random.shuffle(hosts)

    total_v2_bw = 0.0
    for host in hosts:
        if total_v2_bw / total_bw >= torv2_bw_up_frac:
            break
        host_config = config['hosts'][host]
        (bw, unit) = host_config['bandwidth_up'].split()
        # We only handle everything using the same unit
        assert(unit == 'kilobit')
        total_v2_bw += float(bw)
        for process in host_config['processes']:
            # Change tor process to use the secondary binary
            if process['path'].endswith('/tor'):
                process['path'] = f'{ci_project_dir}/jobs/opt/torv2/bin/tor'

    yaml.dump(config, stream=open(shadow_config_path, 'w'))

def apply_asan_workaround(shadow_config_path):
    config = yaml.load(open(shadow_config_path), yaml.loader.Loader)

    for (host, host_config) in config['hosts'].items():
        for process in host_config['processes']:
            # Ensure environment key exists
            if 'environment' not in process:
                process['environment'] = {}

            # Tell asan not to validate link order, since shadow's LD_PRELOAD's
            # libraries. These shouldn't interfere with asan, since we don't
            # override free, malloc, etc.
            #
            # Should be a no-op when asan is not in use.
            #
            # https://github.com/google/sanitizers/issues/796#issuecomment-578073928
            process['environment']['ASAN_OPTIONS'] = 'verify_asan_link_order=0'

    yaml.dump(config, stream=open(shadow_config_path, 'w'))

def add_5MB_uploads():
    info("Adding 5 MiB uploads to perf clients")
    # We use a glob here to handle both older versions of tornettools that
    # didn't have hidden services, and newer versions, that do. In newer
    # versions this config file is split into a `-hs` and `-exit` version.
    for filename in glob.glob(f'{getenv("SIMDIR")}/conf/tgen-perf*.tgenrc.graphml'):
        info(f'Adding uploads to {filename}')
        g = networkx.read_graphml(filename)
        g.add_node('stream_5m_up', sendsize='5 MiB', recvsize='1000 bytes', stallout='0 seconds', timeout='120 seconds')
        g.add_edge('pause', 'stream_5m_up', weight='1.0')
        networkx.write_graphml(g, filename)

def glob1(pattern):
    rv = glob.glob(pattern)
    assert(len(rv) == 1)
    return rv[0]

def main():
    # Enable core dumps
    resource.setrlimit(resource.RLIMIT_CORE, (resource.RLIM_INFINITY, resource.RLIM_INFINITY))

    random.seed(getenv('TRIAL_NUM'))

    match (bool(getenv('PL_PUSH_RESULTS', '')), bool(getenv('RESULTS_PUSH_TOKEN', ''))):
        case True, True:
            info("PL_PUSH_RESULTS and RESULTS_PUSH_TOKEN set; we'll push results")
        case True, False:
            warning(f"{WARNING}PL_PUSH_RESULTS set, but RESULTS_PUSH_TOKEN isn't; results will not be pushed.{ENDC}")
        case False, _:
            info("PL_PUSH_RESULTS unset; we won't push results")

    # Generate sim config
    run('''apt-get install -y bsdmainutils faketime libevent-dev libssl-dev python3 python3-pip python3-pyparsing wget xz-utils zlib1g-dev''')
    run('''pip3 install -r jobs/src/tornettools/requirements.txt --break-system-packages''')
    run('''pip3 install -I jobs/src/tornettools --break-system-packages''')

    # We're abusing cache here to persist 'full' results locally.  We don't
    # actually want results from a previous run though, so remove it if
    # necessary.
    run('''rm -rf $SIMDIR''')
    run('''mkdir -p `dirname $SIMDIR`''')

    # Serve current SIMDIR over onion service for live monitoring.
    run('''apt install -y tor''')    # Use stock tor
    # Single hop, non-anonymous, services
    run('''echo "SocksPort 0" >> /etc/tor/torrc''')
    run('''echo "HiddenServiceNonAnonymousMode 1" >> /etc/tor/torrc''')
    run('''echo "HiddenServiceSingleHopMode 1" >> /etc/tor/torrc''')
    # simdir service
    run('''echo "HiddenServiceDir /var/lib/tor/simdir/" >> /etc/tor/torrc''')
    run('''echo "HiddenServicePort 80 127.0.0.1:8081" >> /etc/tor/torrc''')
    bg('''cd $(dirname $SIMDIR) && python3 -m http.server 8081 &> simdir-http.log''')
    # Run tor
    bg('''/usr/bin/tor &> tor.log''')
    # Print hostnames once they've appeared
    run('''while [ ! -f /var/lib/tor/simdir/hostname ]; do echo "Waiting for simdir service"; sleep 1; done''')
    run('''echo simdir service: $(cat /var/lib/tor/simdir/hostname)''')

    params = [
        'tornettools',
        '--seed', getenv('TRIAL_NUM'),
        'generate',
        glob1('jobs/network-data/relayinfo_staging_*.json'),
        glob1('jobs/network-data/userinfo_staging_*.json'),
        'jobs/network-data/networkinfo_staging.gml',
        'jobs/network-data/tmodel',
        '--network_scale', getenv('SCALE'),
        '--prefix', getenv('SIMDIR'),
        '--tor', 'jobs/opt/tor/bin/tor',
        '--torgencert', 'jobs/opt/tor/bin/tor-gencert',
        '--process_scale', getenv('PROCESS_SCALE'),
        '--load_scale', getenv('LOAD_SCALE'),
    ]
    # Add optional flags, which aren't supported in all versions of tornettools
    for (envname, flagname) in [
            ('PL_ONION_SERVICE_USER_SCALE', '--onion_service_user_scale'),
            ('PL_TORPERF_NUM_ONION_SERVICE', '--torperf_num_onion_service'),
            ('PL_TORPERF_NUM_EXIT', '--torperf_num_exit')]:
        value = getenv(envname)
        if value != '':
            params.extend([flagname, value])

    # Optionally add the --onion_service_user_scale flag. The baseline version
    # of tornettools doesn't support this flag, so we don't add it by default.
    run(params)

    # Run simulation
    run('''apt-get install -y libevent-2.1-7 libssl3 zlib1g libglib2.0-0 libigraph3 stow sysstat python3 python3-pip procps''')
    run('''pip3 install -r jobs/src/tornettools/requirements.txt --break-system-packages''')
    run('''pip3 install -I jobs/src/tornettools --break-system-packages''')
    run('''mkdir $HOME/.local''')
    run('''stow -d jobs/opt -t $HOME/.local oniontrace''')
    run('''stow -d jobs/opt -t $HOME/.local tgen''')
    run('''stow -d jobs/opt -t $HOME/.local tor''')
    # TODO: Collect trace data from more tor instances; working on space issues.
    # https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/issues/4
    # - echo "Log [sim]debug file $CI_PROJECT_DIR/jobs/tornet/$TRIAL_NUM/tor-trace.txt" >> jobs/tornet/$TRIAL_NUM/conf/tor.relay.exitonly.torrc

    with open(getenv('SIMDIR') + '/conf/tor.common.torrc', 'a') as torrc:
        torrc.write('\n')
        torrc.write('\n'.join([
            # Record all warnings and errors.
            f'Log warn-warn file {getenv("SIMDIR")}/tor-warn.txt',
            f'Log err file {getenv("SIMDIR")}/tor-err.txt',
            # Log protocol warnings
            'ProtocolWarnings 1',
            ]))
        torrc.write('\n')
        torrc.write(getenv('PL_TORRC_COMMON_SUFFIX'))

    # Set simulation time
    run('''sed -i "s/stop_time:.*/stop_time: $SIM_TIME/" $SIMDIR/shadow.config.yaml''')

    # Configure events to log in onionperf
    run('''sed -i 's/Events=[^\s]*/Events=BW,CIRC,CIRC_MINOR,STREAM,GUARD,BUILDTIMEOUT_SET/' $SIMDIR/shadow.config.yaml''')

    # Enable guards.
    # FIXME: Also need to set process scale to 1.0. See
    # https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/issues/5
    run(['sed', '-i', 's/UseEntryGuards.*/UseEntryGuards 1/', f"{getenv('SIMDIR')}/conf/tor.client.torrc"])
    # Also enable guards for onionservices, if using a tornettools that supports onionservices.
    if os.path.exists(f"{getenv('SIMDIR')}/conf/tor.onionservice.torrc"):
        run(['sed', '-i', 's/UseEntryGuards.*/UseEntryGuards 1/', f"{getenv('SIMDIR')}/conf/tor.onionservice.torrc"])
        # Add onion service torrc options
        with open(getenv('SIMDIR') + '/conf/tor.onionservice.torrc', 'a') as torrc:
           torrc.write('\n')
           torrc.write(getenv('PL_TORRC_ONIONSERVICE_SUFFIX'))

    # Override authority configs with recent consensus params, from
    # https://consensus-health.torproject.org/#consensusparams:
    # "Consensus was published 2021-12-10 18:00:00 UTC"
    # TODO: have tornettools do this for us from its consensus data?
    run('''for param in $PL_CONSENSUS_PARAMS
    do
      echo ConsensusParams $param >> $SIMDIR/conf/tor.relay.authority.torrc
    done
    ''')

    apply_bandwidth_overrides(
            f'{getenv("SIMDIR")}/shadow.config.yaml',
            {
                'perfclient':
                  {'up':   getenv('PL_PERFCLIENT_BANDWIDTH_UP'),
                   'down': getenv('PL_PERFCLIENT_BANDWIDTH_DOWN') },
                'server':
                  {'up':   getenv('PL_SERVER_BANDWIDTH_UP'),
                   'down': getenv('PL_SERVER_BANDWIDTH_DOWN') },
            })

    apply_torv2_overrides(
            f'{getenv("SIMDIR")}/shadow.config.yaml',
            host_pattern=r'relay.*exit.*',
            torv2_bw_up_frac=float(getenv("PL_TORV2_EXIT_BW_UP_FRAC")),
            ci_project_dir=getenv('CI_PROJECT_DIR'))

    apply_torv2_overrides(
            f'{getenv("SIMDIR")}/shadow.config.yaml',
            host_pattern=r'markovclient.*exit',
            torv2_bw_up_frac=float(getenv("PL_TORV2_BG_CLIENT_BW_UP_FRAC")),
            ci_project_dir=getenv('CI_PROJECT_DIR'))

    apply_torv2_overrides(
            f'{getenv("SIMDIR")}/shadow.config.yaml',
            host_pattern=r'markovclient.*onionservice',
            torv2_bw_up_frac=float(getenv("PL_TORV2_BG_CLIENT_BW_UP_FRAC")),
            ci_project_dir=getenv('CI_PROJECT_DIR'))

    apply_asan_workaround(f'{getenv("SIMDIR")}/shadow.config.yaml')

    # Extend perf clients to also do 5 MiB uploads
    if getenv('ADD_5_MB_UPLOADS', False):
        add_5MB_uploads()

    # Start monitoring in background
    bg('''$CI_PROJECT_DIR/monitor.sh $SIMDIR/sysinfo $SIMDIR &> $SIMDIR/monitoring.txt''')
    bg('''$CI_PROJECT_DIR/log_mappings.py 300 > $SIMDIR/mappings.txt''')

    # - PARALLELISM=$((`nproc` / 2))
    # The shadow runner has 2 numa nodes, each with 20 physical cores. Shadow is currently not numa-aware
    # when migrating tasks across workers; e.g. it doesn't try to avoid stealing from another numa node, and
    # it doesn't reassign physical pages to the new node. It *may* be beneficial to restrict to a single
    # numa node, but probably not at the cost of half the cores.
    parallelism = int(check_output("lscpu --online --parse=CORE | grep -v '#' | sort | uniq | wc -l"))
    shadow_args = {
        # Performance
        '--use-cpu-pinning': 'true',
        '--parallelism': str(parallelism),

        # Better simulation performance, potentially at the cost of some accuracy
        '--runahead': '2ms',

        # Debugging
        '--use-syscall-counters': 'true',
        '--progress': 'true',

        # Plumbing
        '--template-directory': 'shadow.data.template',
        '--seed': getenv('TRIAL_NUM'),
    }
    shadow_kwarg_overrides = getenv('PL_SHADOW_KWARG_OVERRIDES')
    if shadow_kwarg_overrides:
        for arg in shadow_kwarg_overrides.split(','):
            k, v = arg.split('=')
            shadow_args[k] = v
    shadow_args_list = [f'{k}={v}' for (k, v) in shadow_args.items()]
    run([
        'tornettools',
        '--seed', getenv('TRIAL_NUM'),
        'simulate',
        '--shadow', getenv('CI_PROJECT_DIR') + '/jobs/opt/shadow/bin/shadow',
        '--args', ' '.join(shadow_args_list),
        getenv('SIMDIR'),
    ])

    # Save all pipeline param vars (PL_), and an allow-list of gitlab
    # built-in env vars.
    # While it'd be useful to save all env variables, it's too tricky to filter
    # out ones with sensitive info.
    run('''for v in CI_COMMIT_BRANCH CI_COMMIT_REF_NAME CI_COMMIT_REF_SLUG CI_COMMIT_SHA CI_COMMIT_SHORT_SHA CI_JOB_ID CI_JOB_IMAGE CI_JOB_STATUS CI_JOB_URL CI_JOB_STARTED_AT CI_PIPELINE_ID CI_PIPELINE_IID CI_PIPELINE_URL CI_PIPELINE_CREATED_AT CI_PROJECT_ID CI_PROJECT_NAME CI_PROJECT_NAMESPACE CI_PROJECT_PATH_SLUG CI_PROJECT_PATH CI_PROJECT_URL CI_RUNNER_DESCRIPTION CI_RUNNER_EXECUTABLE_ARCH CI_RUNNER_ID CI_RUNNER_REVISION CI_RUNNER_TAGS; do echo $v=${!v:-} >> $SIMDIR/env; done''')
    run('''env | grep ^PL_ | sort >> $SIMDIR/env''')

    # Parse simulation results.
    # The raw results are too large to save as an artifact.
    # Parse the logs and save the results of that.
    # https://gitlab.torproject.org/tpo/tpa/team/-/issues/40340
    run('''pip3 install -r jobs/src/tgen/tools/requirements.txt --break-system-packages''')
    run('''pip3 install -I jobs/src/tgen/tools --break-system-packages''')
    run('''pip3 install -r jobs/src/oniontrace/tools/requirements.txt --break-system-packages''')
    run('''pip3 install -I jobs/src/oniontrace/tools --break-system-packages''')
    run('''tornettools parse -c $CONVERGENCE_TIME_S $SIMDIR''')

    # Download previous results
    run('''git clone --depth=1 https://gitlab.torproject.org/jnewsome/sim-results.git''')

    # Plot
    run('''tornettools plot --tor_metrics_path $CI_PROJECT_DIR/jobs/network-data/tor_metrics_*.json --prefix $SIMDIR/plots -l $PL_BASELINE_LABEL experiment -- $CI_PROJECT_DIR/sim-results/$PL_BASELINE_DIR/tornet $SIMDIR''')

    # Separate exits and relay logs into separate files, for stats aggregation
    run('''find $SIMDIR/shadow.data/hosts -wholename *relay*tor*stdout ! -name '*exit*' | xargs cat > $SIMDIR/tor-nonexit-warns.log''')
    run('''find $SIMDIR/shadow.data/hosts -wholename *exit*tor*stdout | xargs cat > $SIMDIR/tor-exit-warns.log''')
    run('''find $SIMDIR/shadow.data/hosts -wholename *server*onionservice*tor*stdout | xargs cat > $SIMDIR/tor-svc-warns.log''')

    # Process congestion control statistics
    run('''apt-get install -y pypy3''')
    run('''grep "CIRC=" $SIMDIR/tor-exit-warns.log > $SIMDIR/circ-cc.log || true''')
    run('''grep "CIRC=" $SIMDIR/tor-svc-warns.log > $SIMDIR/circ-cc-svc.log || true''')
    run('''$CI_PROJECT_DIR/cc_stats.py $SIMDIR/circ-cc.log > $SIMDIR/cc-stats.log.txt''')
    run('''$CI_PROJECT_DIR/cc_stats.py $SIMDIR/circ-cc-svc.log > $SIMDIR/cc-stats-onion.log.txt''')

    # Get TLS overhead
    run('''grep "Average packaged cell fullness" $SIMDIR/tor-nonexit-warns.log > $SIMDIR/heartbeat-nonexit.log || true''')
    run('''grep "Average packaged cell fullness" $SIMDIR/tor-exit-warns.log > $SIMDIR/heartbeat-exit.log || true''')
    run('''$CI_PROJECT_DIR/tls_overhead.py $SIMDIR/heartbeat-nonexit.log  >> $SIMDIR/cc-stats.log.txt''')
    run('''$CI_PROJECT_DIR/tls_overhead.py $SIMDIR/heartbeat-exit.log >> $SIMDIR/cc-stats.log.txt''')

    # Get misc bad stuff
    run('''egrep "BADSTUFF|Bug:|\[err\]|clock has been stalled|cells in its queue" $SIMDIR/tor-nonexit-warns.log > $SIMDIR/bad-stuff.log.txt || true''')
    run('''egrep "BADSTUFF|Bug:|\[err\]|clock has been stalled|cells in its queue" $SIMDIR/tor-exit-warns.log >> $SIMDIR/bad-stuff.log.txt || true''')
    run('''egrep "BADSTUFF|Bug:|\[err\]|clock has been stalled|cells in its queue" $SIMDIR/tor-svc-warns.log >> $SIMDIR/bad-stuff.log.txt || true''')
    run('''find $SIMDIR/shadow.data/hosts -wholename *client*stdout | xargs egrep -H "BADSTUFF|Bug:|\[err\]" >> $SIMDIR/bad-stuff.log.txt || true''')

    # Save results (if we have the access token)
    run('''git config --global user.email "none"''')
    run('''git config --global user.name "sponsor-61-pipeline"''')
    run('''
    RESULTS_DIR=`date -d "$CI_PIPELINE_CREATED_AT" +%F`-$CI_COMMIT_REF_NAME-${PL_SLUG:-}-$CI_PIPELINE_ID/tornet/$TRIAL_NUM
    mkdir -p sim-results/$RESULTS_DIR
    echo $CI_PIPELINE_URL > sim-results/$RESULTS_DIR/pipeline_url
    echo $CI_JOB_URL > sim-results/$RESULTS_DIR/job_url
    cp $SIMDIR/env sim-results/$RESULTS_DIR
    cp -r $SIMDIR/tornet.plot.data sim-results/$RESULTS_DIR
    cp $SIMDIR/bad-stuff.log.txt sim-results/$RESULTS_DIR
    cp $SIMDIR/cc-stats.log.txt sim-results/$RESULTS_DIR
    cp $SIMDIR/cc-stats-onion.log.txt sim-results/$RESULTS_DIR
    cp $SIMDIR/*json* sim-results/$RESULTS_DIR
    cp $SIMDIR/shadow.config.yaml sim-results/$RESULTS_DIR
    git -C sim-results add $RESULTS_DIR
    git -C sim-results commit -m "Auto-commit for $RESULTS_DIR"
    ''')

    # This loop could be better written as a parser in python.
    # Find the node corresponding to the perfclient from the shadow hosts
    # Extract the country code with a bit of grep magic
    shadow_config = yaml.load(open(getenv('SIMDIR') + '/shadow.config.yaml'), yaml.loader.Loader)
    shadow_network = networkx.read_gml(getenv('CI_PROJECT_DIR') + '/jobs/network-data/networkinfo_staging.gml')
    for (host, host_config) in shadow_config['hosts'].items():
        if host.startswith("perfclient"):
            node = str(host_config['network_node_id'])
            code = shadow_network.nodes[node]['country_code'].lower()
            debug(f"perflclient '{host}' has node '{node}' and code '{code}'")
            run(f'mkdir -p $SIMDIR/{code}/shadow.data/hosts')
            run(f'cp -r $SIMDIR/shadow.data/hosts/{host} $SIMDIR/{code}/shadow.data/hosts/')
    # Plot tornettools
    run('''mkdir -p $SIMDIR/plots''')
    run('''tornettools plot --tor_metrics_path $CI_PROJECT_DIR/jobs/network-data/tor_metrics_*.json --prefix $SIMDIR/plots/ -l $PL_BASELINE_LABEL experiment -- $CI_PROJECT_DIR/sim-results/$PL_BASELINE_DIR/tornet $SIMDIR''')

    # Parse and plot tornettools per country code DE/HK
    # Check that we have data for a given country code from shadow
    # Check that we have data for a given country code from onionperf archives from the live network
    # Finally push sim results to the sim-results repository
    run('''
    for CC in $COUNTRY_CODES; do
      if [ -d "$SIMDIR/$CC" ]; then
        if [ -d "$CI_PROJECT_DIR/jobs/network-data/$CC" ]; then
          mkdir -p $SIMDIR/plots/$CC
          tornettools parse -c $CONVERGENCE_TIME_S $SIMDIR/$CC
          tornettools plot --tor_metrics_path $CI_PROJECT_DIR/jobs/network-data/$CC/tor_metrics_*.json --prefix $SIMDIR/plots/$CC -l $PL_BASELINE_LABEL experiment -- $CI_PROJECT_DIR/sim-results/$PL_BASELINE_DIR/tornet-$CC $SIMDIR/$CC
          RESULTS_DIR_CC=`date -d "$CI_PIPELINE_CREATED_AT" +%F`-$CI_COMMIT_REF_NAME-${PL_SLUG:-}-$CI_PIPELINE_ID/tornet-$CC/$TRIAL_NUM
          mkdir -p sim-results/$RESULTS_DIR_CC
          cp -r $SIMDIR/$CC/tornet.plot.data sim-results/$RESULTS_DIR_CC
          cp $SIMDIR/$CC/*json* sim-results/$RESULTS_DIR_CC
          git -C sim-results add $RESULTS_DIR_CC
          git -C sim-results commit -m "Auto-commit for $RESULTS_DIR_CC"
        fi
      fi
    done''')

    push_token = getenv('RESULTS_PUSH_TOKEN', '')
    if getenv('PL_PUSH_RESULTS', '') and push_token:
        run(['git',
            '-C', 'sim-results',
            'remote',
            'set-url', 'origin', f'https://pipeline:{push_token}@gitlab.torproject.org/jnewsome/sim-results.git'])
        # Try a few times:
        for i in range(5):
            run(['git', '-C', 'sim-results', 'pull'])
            try:
                run(['git', '-C', 'sim-results', 'push'])
                break
            except:
                pass
            # Exponential backoff and try again
            backoff = random.randint(0, 2**i)
            info(WARNING + f"Push attempt {i} failed. Sleeping for {backoff} seconds")
            time.sleep(random.randint(0, 2**i))
    else:
        info(WARNING + "No push token. Results will not be archived")

if __name__ == '__main__':
    main()
