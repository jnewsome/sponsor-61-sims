#!/bin/bash

set -euo pipefail

# Show commands being run
set -x

SYSINFO=$1
TORNET=$2

mkdir -p $SYSINFO
cat /sys/devices/system/cpu/vulnerabilities/* | tee $SYSINFO/cpu-vulns.txt
lscpu | tee $SYSINFO/lscpu.txt
lscpu --online --parse=CPU,CORE,SOCKET,NODE | tee $SYSINFO/lscpu-parse.txt
uname -a | tee $SYSINFO/uname.txt
cat /proc/sys/kernel/pid_max | tee $SYSINFO/pid_max
cat /proc/sys/vm/max_map_count | tee $SYSINFO/max_map_count
ulimit -aS | tee $SYSINFO/ulimit.soft.txt
ulimit -aH | tee $SYSINFO/ulimit.hard.txt
cat /proc/sys/kernel/threads-max | tee $SYSINFO/threads-max
cat /proc/sys/fs/file-max | tee $SYSINFO/file-max

# log cgroup limits
CGROUP_ROOT=/sys/fs/cgroup
CGROUP=$(cat /proc/self/cgroup | cut -d : -f3)
CGROUP_DIR="$CGROUP_ROOT$CGROUP"
while [[ "$CGROUP_DIR" == "$CGROUP_ROOT"* ]]
do
  echo "cgroup dir: $CGROUP_DIR" | tee -a "$SYSINFO"/cgroups
  for f in "$CGROUP_DIR"/*.max
  do
    if [ -f "$f" ]
    then
      base=$(basename "$f")
      value=$(cat "$f")
      echo "  $base: $value" | tee -a "$SYSINFO"/cgroups
    fi
  done
  CGROUP_DIR=$(dirname "$CGROUP_DIR")
done

# Run specified command in background and grab pid
# "$@" > /dev/null &
ulimit -c unlimited

while true
do
  sleep 300
  echo
  date

  # processes by cpu
  # ps T --sort -pcpu -o comm,s,nlwp,pcpu,cputime,rss,wchan | head -n4
  # processes by rss
  # ps T --sort -rss -o comm,s,nlwp,pcpu,cputime,rss,wchan | head -n4

  # load averages
  # uptime
  # memory
  # free -h
  # disk
  echo df
  df -h
  echo

  #mpstat -P ALL 1 1
  echo top
  top -b1 -n1 | head -n $((`nproc` + 10))
  echo

  echo thread summary:
  ps xH -o s,comm,wchan | sed 's/[0-9]/ /g' | sort -s | uniq -c | sort -n -s || true
  echo

  # All warnings and errors (except known noisy ones)
  #echo warnings:
  #grep -E 'WARN|ERR' $TORNET/shadow.log | grep -v "we only support AF_INET" | grep -v -E 'Ignoring `sigaction` for signal (31|11)' || echo none
  #echo

  # Last log line; typically including current sim time
  echo last shadow log line:
  tail -n1 $TORNET/shadow.log || echo none
done
